<?php

require_once 'inc/config.php';
require_once 'inc/functions.php';
require_once 'inc/ShoppingCart.php';

// Insantiate the shopping cart
$my_cart = new ShoppingCart();

$title = "Checkout";

if (isset($_POST) && !empty($_POST)) {
  $_SESSION['checkout_values'] = $_POST;
}

if(!isset($_SESSION['logged_in']) || $_SESSION['logged_in'] == false) {
  $_SESSION['checkout'] = true;
  header('Location: login.php?logout=true');
}

if (!empty($my_cart->cart)) {
  // Create query to get product info
  $query = "SELECT product_id, artist, album_title, price, cover_img
            FROM product
            WHERE product_id
            IN (".implode(',',array_keys($my_cart->cart)).")";

  // Prepare query
  $stmt = $dbh->prepare($query);

  // Execute query
  $stmt->execute();

  // Fetch result
  $cart_items = $stmt->fetchAll(PDO::FETCH_ASSOC);

  // Create query to get customer info
  $query = "SELECT first_name,
                   last_name,
                   email,
                   address,
                   city,
                   postal_code,
                   province,
                   phone
            FROM customer
            WHERE customer_id = ?
            AND email = ?";

  // Prepare params
  $params = array(
    $_SESSION['user_id'],
    $_SESSION['user_email']
  );

  // Prepare query
  $stmt = $dbh->prepare($query);

  // Execute the query
  $stmt->execute($params);

  // Fetch result
  $customer = $stmt->fetch(PDO::FETCH_ASSOC);
}

include('inc/header.php'); ?>

  <div id="wrapper">

    <div id="checkout_content">
      <div id="customer_wrapper">
        <h1>Payment &amp; Shipping Info</h1>


            <form action="thankyou.php" method="POST">
              <table id="customer_table">
                <input type="hidden" name="customer_id" value="<?=$_SESSION['user_id']?>" />
                <input type="hidden" name="email" value="<?=$_SESSION['user_email']?>" />
                <tr>
                  <td>First name: </td>
                  <td><input type="text" readonly name="first_name" value="<?=$customer['first_name']?>"></td>
                </tr>
                <tr>
                  <td>Last name: </td>
                  <td><input type="text" readonly name="last_name" value="<?=$customer['last_name']?>"></td>
                </tr>
                <tr>
                  <td>Shipping address: </td>
                  <td><input type="text" name="address" value="<?=$customer['address']?>"></td>
                </tr>
                <tr>
                  <td>City: </td>
                  <td><input type="text" name="city" value="<?=$customer['city']?>"></td>
                </tr>
                <tr>
                  <td>Postal code: </td>
                  <td><input type="text" name="postal_code" value="<?=$customer['postal_code']?>"></td>
                </tr>
                <tr>
                  <td>Province: </td>
                  <td>
                    <select name="province">
                      <option <?php if ($customer['province']=="AB") echo 'selected'; ?> value="AB">Alberta</option>
                      <option <?php if ($customer['province']=="BC") echo 'selected'; ?> value="BC">British Columbia</option>
                      <option <?php if ($customer['province']=="MB") echo 'selected'; ?> value="MB">Manitoba</option>
                      <option <?php if ($customer['province']=="NB") echo 'selected'; ?> value="NB">New Brunswick</option>
                      <option <?php if ($customer['province']=="NL") echo 'selected'; ?> value="NL">Newfoundland and Labrador</option>
                      <option <?php if ($customer['province']=="NS") echo 'selected'; ?> value="NS">Nova Scotia</option>
                      <option <?php if ($customer['province']=="ON") echo 'selected'; ?> value="ON">Ontario</option>
                      <option <?php if ($customer['province']=="PE") echo 'selected'; ?> value="PE">Prince Edward Island</option>
                      <option <?php if ($customer['province']=="QC") echo 'selected'; ?> value="QC">Quebec</option>
                      <option <?php if ($customer['province']=="SK") echo 'selected'; ?> value="SK">Saskatchewan</option>
                      <option <?php if ($customer['province']=="NT") echo 'selected'; ?> value="NT">Northwest Territories</option>
                      <option <?php if ($customer['province']=="NU") echo 'selected'; ?> value="NU">Nunavut</option>
                      <option <?php if ($customer['province']=="YT") echo 'selected'; ?> value="YT">Yukon</option>
                    </select>
                  </td>
                </tr>
                <tr>
                  <td>Phone: </td>
                  <td><input type="text" name="phone" value="<?=$customer['phone']?>" />
                </tr>

                <tr>
                  <td>Payment type: </td>
                  <td>
                    <select name="payment_type">
                      <option value="visa">Visa</option>
                      <option value="mastercard">Mastercard</option>
                    </select>
                  </td>
                </tr>
                <tr>
                  <td>Name on card: </td>
                  <td><input type="text" name="credit_card_name"></td>
                </tr>
                <tr>
                  <td>Card Number: </td>
                  <td><input type="text" name="credit_card"></td>
                </tr>
                <tr>
                  <td>CV: </td>
                  <td><input type="text" name="cv"></td>
                </tr>
                <tr>
                  <td>Expiry Date: </td>
                  <td><input type="date"></td>
                </table>

                <input type="hidden" name="subtotal" value="<?=number_format($_SESSION['checkout_values']['sub_total'], 2)?>" />
                <input type="hidden" name="tax" value="<?=number_format($_SESSION['checkout_values']['tax'], 2)?>" />
                <input type="hidden" name="total" value="<?=number_format($_SESSION['checkout_values']['total'], 2)?>" />

                <input type="submit" value="Confirm &amp; Pay" />
            </form>
      </div>

      <div id="items_wrapper">
        <?php if(!empty($my_cart->cart)) : ?>
          <table id="cart_items">
            <tr>
              <th colspan="2">Item</th>
              <th>Qty</th>
              <th>Price</th>
            </tr>
            <?php foreach ($cart_items as $row) : ?>
            <tr>
              <td><img src="img/album_cover/<?=$row['cover_img']?>" height="120" width="120" style="padding-bottom: 5px"/></td>
              <td><?=$row['artist']?> - <?=$row['album_title']?></td>
              <td><?=$my_cart->cart[$row['product_id']]?></td>
              <td>$<?=$row['price']?></td>
            </tr>
          <?php endforeach; ?>
          <tr style="border-top: 1px solid #cbcbcb"><th colspan="3">Subtotal</th><td>$<?=number_format($_SESSION['checkout_values']['sub_total'], 2)?></tr>
          <tr><th colspan="3">Tax</th><td>$<?=number_format($_SESSION['checkout_values']['tax'], 2)?></tr>
          <tr><th colspan="3">Total</th><td>$<?=number_format($_SESSION['checkout_values']['total'], 2)?></tr>
          </table>
        <?php endif; ?>
      </div>
    </div>

  </div>

<?php include('inc/footer.php'); ?>
