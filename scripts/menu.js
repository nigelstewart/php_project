var media = window.matchMedia( "(max-width: 959px)" );

if (media.matches){
  $(document).ready(function(){
    $("#menu").hide();
    $("#menu_arrow").click(function(){
      $("#menu").slideToggle(400, 'swing');
      $("#menu_arrow").toggleClass("rotated");
    });
  });
}
