<?php

require_once 'inc/config.php';
require_once 'inc/functions.php';
require_once 'inc/ShoppingCart.php';

// Insantiate the shopping cart
$my_cart = new ShoppingCart();

if (!empty($my_cart->cart)) {
  $query = "SELECT product_id, artist, album_title, price, cover_img
            FROM product
            WHERE product_id
            IN (".implode(',',array_keys($my_cart->cart)).")";

  // Prepare query
  $stmt = $dbh->prepare($query);

  // Execute query
  $stmt->execute();

  // Fetch result
  $cart_items = $stmt->fetchAll(PDO::FETCH_ASSOC);

  // Get line/sub totals
  $subtotal = 0;
  foreach ($cart_items as $item) {
    $lineqty = $my_cart->cart[$item['product_id']];
    $linetotal = number_format($lineqty * $item['price'], 2);
    $subtotal += $linetotal;
  }

  // Get tax
  $tax = number_format($subtotal * .13, 2);

  // Get total
  $total = number_format($subtotal + $tax, 2);
}

if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update_item'])) {
  $product_id = intval($_POST['product_id']);
  $quantity = intval($_POST['qty']);
  $my_cart->updateItem($product_id, $quantity);
  header("Refresh:0");
} elseif($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['clear_cart'])) {
  $my_cart->emptyCart();
}

$title = "Shopping Cart";

include('inc/header.php'); ?>

  <div id="wrapper">

    <!-- Menu Include -->
    <?php include('inc/menu.php'); ?>

    <div id="content">
      <h1 id="cart_header">Your Cart</h1>

      <?php if(!empty($my_cart->cart)) : ?>
        <table id="cart_items">
          <tr>
            <th colspan="2">Item</th>
            <th>Quantity</th>
            <th>Unit Price</th>
          </tr>
          <?php foreach ($cart_items as $row) : ?>
          <tr>
            <td><img src="img/album_cover/<?=$row['cover_img']?>" height="120" width="120" style="padding-bottom: 5px"/></td>
            <td><?=$row['artist']?> - <?=$row['album_title']?></td>
            <td>
              <form id="update_qty" method="post" action="#">
                <select name="qty" id="cart_qty">
                  <option value="0">0</option>
                  <option <?php if($my_cart->cart[$row['product_id']] == 1){echo 'selected';} ?> value="1">1</option>
                  <option <?php if($my_cart->cart[$row['product_id']] == 2){echo 'selected';} ?> value="2">2</option>
                  <option <?php if($my_cart->cart[$row['product_id']] == 3){echo 'selected';} ?> value="3">3</option>
                  <option <?php if($my_cart->cart[$row['product_id']] == 4){echo 'selected';} ?> value="4">4</option>
                  <option <?php if($my_cart->cart[$row['product_id']] == 5){echo 'selected';} ?> value="5">5</option>
                </select>
                <input type="hidden" name="product_id" value="<?=$row['product_id']?>" />
                <input type="hidden" name="update_item" value="update_item" />
                <input type="submit" value="Update" />
              </form>
            </td>
            <td style="text-align:right">$<?=$row['price']?></td>
          </tr>
        <?php endforeach; ?>
        <tr style="border-top: 1px solid #868686"><th colspan="3">Sub Total</th><td style="text-align:right">$<?=$subtotal?></td></tr>
        <tr><th colspan="3">Tax</th><td style="text-align:right">$<?=$tax?></td></tr>
        <tr><th colspan="3">Total</th><td style="text-align:right">$<?=$total?></td></tr>
        </table>

        <div id="cart_buttons">
          <form id="clear_cart" method="post" action="#">
            <input type="hidden" name="clear_cart" value="clear_cart" />
            <input type="submit" value="Clear Cart" />
          </form>
          <form id="checkout" method="post" action="checkout.php">
            <input type="hidden" name="checkout" value="checkout" />
            <input type="hidden" name="sub_total" value="<?=$subtotal?>" />
            <input type="hidden" name="tax" value="<?=$tax?>" />
            <input type="hidden" name="total" value="<?=$total?>" />
            <input type="submit" value="Proceed" />
          </form>
        </div>
      <?php else : ?>
        <p id="cart_empty">You have nothing in your cart.</p>
      <?php endif; ?>

    </div>

  </div>

<?php include('inc/footer.php'); ?>
