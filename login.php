<?php

require_once 'inc/config.php';
require_once 'inc/functions.php';
require_once 'inc/ShoppingCart.php';

// Insantiate the shopping cart
$my_cart = new ShoppingCart();

$title = "Log in";

if (isset($_GET['logout'])) {
  $_SESSION['user_id'] = '';
  $_SESSION['user_email'] = '';
  $_SESSION['user_name'] = '';
  $_SESSION['logged_in'] = false;
  session_regenerate_id();
  $login_msg = "Please log in to continue.";
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

  if (!isset($_POST['email']) || empty($_POST['email'])) {
    $login_msg = "Please enter your email.";
  } elseif (!isset($_POST['password']) || empty($_POST['password'])) {
    $login_msg = "You didn't enter a password.";
  }

  $user_email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
  $user_password = $_POST['password'];

  if(!isset($login_msg) && empty($login_msg)) {
    // Query database for user info
    $query = "SELECT *
              FROM customer
              WHERE email = (?)";

    // Create params
    $params = array($user_email);

    // Prepare query
    $stmt = $dbh->prepare($query);

    //Check if execute query
    if($stmt->execute($params)){
      // Fetch user
      $user = $stmt->fetch(PDO::FETCH_ASSOC);
      if(!$user || !password_verify($user_password, $user['password'])) {
        $_SESSION['logged_in'] = false; // Set session logged_in to false
        $login_msg = "Invalid email/password combination.";
      } else {
        $_SESSION['logged_in'] = true; // Set session logged_in to true
        $_SESSION['user_id'] = $user['customer_id'];
        $_SESSION['user_email'] = $user['email'];
        $_SESSION['user_name'] = $user['first_name'];
        session_regenerate_id();

        if(isset($_SESSION['checkout']) && $_SESSION['checkout'] == true) {
          header('Location: checkout.php');
          exit;
        } else {
          header('Location: profile.php');
          exit;
        }
      }
    } // End if execute query
  } // End if no login msg
} // End if POST

include('inc/header.php'); ?>

  <div id="wrapper">

    <!-- Menu Include -->
    <?php include('inc/menu.php'); ?>

    <div id="content">
      <div id="form_wrapper">
        <?php if (isset($login_msg)) : ?>
          <div id="login_messages">
            <?= $login_msg ?>
          </div>
        <?php endif; ?>
        <form id="login_form" action="login.php" method="POST">
          <p>
            <label for="email">Email: </label>
            <input type="text" name="email" value="<?php if(isset($user_email)){ echo $user_email; } ?>"/>
          </p>
          <p>
            <label for="password">Password: </label>
            <input type="password" name="password" />
          </p>
          <p id="login_button"><input type="submit" Value="Log In" /></p>
        </form>

        <p id="sign_up_link">Don't have an account?<br />Sign up <a href="sign_up.php">here.</a></p>
      </div>
    </div>

  </div>

  <script>
      $(document).ready(function() {
          $("#login_messages").hide()
                  .slideDown()
                  .delay(2000)
                  .slideUp('slow')
      });
  </script>

<?php include('inc/footer.php'); ?>
