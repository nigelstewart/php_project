<?php

require_once 'inc/config.php';
require_once 'inc/functions.php';
require_once 'inc/ShoppingCart.php';

// Insantiate the shopping cart
$my_cart = new ShoppingCart();

if ($_SESSION['logged_in'] === true) {
  $title = $_SESSION['user_name'] . "'s Profile";

  // Get Profile info from database
  // Create query
  $query = "SELECT first_name, last_name, address, city, postal_code, province,
                   country, phone, email
            FROM customer
            WHERE customer_id = {$_SESSION['user_id']}";

  // Prepare query
  $stmt = $dbh->prepare($query);

  // Execute query
  $stmt->execute();

  // Fetch results
  $profile_info = $stmt->fetchAll(\PDO::FETCH_ASSOC);
} else {
  header('Location: login.php?logout=true');
  exit;
}

include('inc/header.php'); ?>
  <div id="success_msg">You are logged in!</div>

  <div id="wrapper">
    <!-- Menu Include -->
    <?php include('inc/menu.php'); ?>

    <div id="content">
      <h1 id="profile_title"><?=$title?></h1>

      <div id="profile_info">

        <?php foreach ($profile_info[0] as $key=>$value) : ?>
          <?php $key = str_replace('_', ' ', $key); ?>
          <?php $key = ucwords($key); ?>
          <?php echo "<p>$key: <span class='post'>$value</span></p>"; ?>
        <?php endforeach; ?>

      </div>
    </div>
  </div>

  <script>
      $(document).ready(function() {
          $("#success_msg").hide()
                  .slideDown()
                  .delay(2000)
                  .slideUp('slow')
      });
  </script>

<?php include('inc/footer.php'); ?>
