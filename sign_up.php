<?php

require_once 'inc/config.php';
require_once 'inc/functions.php';
require_once 'inc/Validator.php';
require_once 'inc/ShoppingCart.php';

// Insantiate the shopping cart
$my_cart = new ShoppingCart();

// Set empty default values for fields
$provinces = array(
  'province' => ''
);

// Instantiate new validator as $v
$v = new Validator();

// Make sure there is a POST submission and required fields are
// valid and not empty
if($_SERVER['REQUEST_METHOD'] == 'POST'){
  // VALIDATE FUNCTION
  validate_registration_form($v);

  // Get sticky for selects
  $provinces = get_sticky_values($provinces);

  // If we have no errors, submit the user to DB
  if(!$v->errors()) {
    submitForm();
  }
}

// Set title
$title = "Sign Up Now";

include('inc/header.php'); ?>

  <div id="wrapper">

    <!-- Menu Include -->
    <?php include('inc/menu.php'); ?>

    <div id="content">
      <form id="sign_up_form" action="#" method="post">
        <h1 id="sign_up_now"><?= "$title" ?> </h1>

        <p>
        <label class="required" for="first_name">First name: </label>
        <input type="text" name="first_name" value="<?=($v->clean('first_name')) ? $v->clean('first_name') : '' ?>" />
        </p><p><br /><br />
        <span class="error"><?php if (isset($v->errors['first_name'])){echo $v->errors['first_name'];} ?></span>
        </p>

        <p>
        <label class="required" for="last_name">Last name: </label>
        <input type="text" name="last_name" value="<?=($v->clean('last_name')) ? $v->clean('last_name') : '' ?>" />
        </p><p><br /><br />
        <span class="error"><?php if (isset($v->errors['last_name'])){echo $v->errors['last_name'];} ?></span>
        </p>

        <p>
        <label class="required" for="address">Address: </label>
        <input type="text" name="address" value="<?=($v->clean('address')) ? $v->clean('address') : '' ?>" />
        </p><p><br /><br />
        <span class="error"><?php if (isset($v->errors['address'])){echo $v->errors['address'];} ?></span>
        </p>

        <p>
        <label class="required" for="city">City: </label>
        <input type="text" name="city" value="<?=($v->clean('city')) ? $v->clean('city') : '' ?>" />
        </p><p><br /><br />
        <span class="error"><?php if (isset($v->errors['city'])){echo $v->errors['city'];} ?></span>
        </p>

        <p>
        <label class="required" for="postal_code">Postal Code: </label>
        <input type="text" name="postal_code" value="<?=($v->clean('postal_code')) ? $v->clean('postal_code') : '' ?>" />
        </p><p><br /><br />
        <span class="error"><?php if (isset($v->errors['postal_code'])){echo $v->errors['postal_code'];} ?></span>
        </p>

        <p>
        <label class="required" for="province">Province: </label>
        <select name="province">
        	<option <?php if ($provinces['province']=="AB") echo 'selected'; ?> value="AB">Alberta</option>
        	<option <?php if ($provinces['province']=="BC") echo 'selected'; ?> value="BC">British Columbia</option>
        	<option <?php if ($provinces['province']=="MB") echo 'selected'; ?> value="MB">Manitoba</option>
        	<option <?php if ($provinces['province']=="NB") echo 'selected'; ?> value="NB">New Brunswick</option>
        	<option <?php if ($provinces['province']=="NL") echo 'selected'; ?> value="NL">Newfoundland and Labrador</option>
        	<option <?php if ($provinces['province']=="NS") echo 'selected'; ?> value="NS">Nova Scotia</option>
        	<option <?php if ($provinces['province']=="ON") echo 'selected'; ?> value="ON">Ontario</option>
        	<option <?php if ($provinces['province']=="PE") echo 'selected'; ?> value="PE">Prince Edward Island</option>
        	<option <?php if ($provinces['province']=="QC") echo 'selected'; ?> value="QC">Quebec</option>
        	<option <?php if ($provinces['province']=="SK") echo 'selected'; ?> value="SK">Saskatchewan</option>
        	<option <?php if ($provinces['province']=="NT") echo 'selected'; ?> value="NT">Northwest Territories</option>
        	<option <?php if ($provinces['province']=="NU") echo 'selected'; ?> value="NU">Nunavut</option>
        	<option <?php if ($provinces['province']=="YT") echo 'selected'; ?> value="YT">Yukon</option>
        </select>
        </p><p><br /><br />
        <span class="error"><?php if (isset($v->errors['province'])){echo $v->errors['province'];} ?></span>
        </p>

        <p>
        <label class="required" for="phone">Country: </label>
        <input type="text" name="country" value="Canada" readonly />
        </p><p><br /><br />
        <span class="error"><?php if (isset($v->errors['country'])){echo $v->errors['country'];} ?></span>
        </p>

        <p>
        <label class="required" for="phone">Phone: </label>
        <input type="text" name="phone" value="<?=($v->clean('phone')) ? $v->clean('phone') : '' ?>" />
        </p><p><br /><br />
        <span class="error"><?php if (isset($v->errors['phone'])){echo $v->errors['phone'];} ?></span>
        </p>

        <p>
        <label class="required" for="email">Email: </label>
        <input type="text" name="email" value="<?=($v->clean('email')) ? $v->clean('email') : '' ?>" />
        </p><p><br /><br />
        <span class="error"><?php if (isset($v->errors['email'])){echo $v->errors['email'];} ?></span>
        </p>

        <p>
        <label class="required" for="password">Password: </label>
        <input type="password" name="password" value="<?=($v->clean('password')) ? $v->clean('password') : '' ?>" />
        </p><p><br /><br />
        <span class="error"><?php if (isset($v->errors['password'])){echo $v->errors['password'];} ?></span>
        </p>

        <p>
        <label class="required" for="confirm_password">Confirm Password: </label>
        <input type="password" name="confirm_password" value="<?=($v->clean('confirm_password')) ? $v->clean('confirm_password') : '' ?>" />
        </p><p><br /><br />
        <span class="error"><?php if (isset($v->errors['confirm_password'])){echo $v->errors['confirm_password'];} ?></span>
        </p>

        <p id="buttons"><input type="submit" value="Sign Up" />&nbsp;&nbsp;<input type="reset" value="Start Over" /></p>
      </form>
    </div>

  </div>

<?php include('inc/footer.php'); ?>
