<div id="menu">
  <form id="search_bar" action="shop.php" method="get">
    <input id="sbar" type="text" name="search" placeholder="Search the shop" />
    <input id="sbutton" type="submit" value="Search" />
  </form>

  <ul>
    <li><a href="index.php" <?= $title == 'Base 16 Records' ? 'class="current_page"' : ''?> >Home</a></li>
    <li>
      <a href="cart.php" <?= $title == 'Shopping Cart' ? 'class="current_page"' : ''?> >
      Cart <i class="fa fa-shopping-cart" aria-hidden="true"></i>
      <span style="color:#b6b6b6;"> (<?= getQty($my_cart) ?>)</span></a>
    </li>
    <li><a href="shop.php" <?= $title == 'Shop' ? 'class="current_page"' : ''?> >Shop All</a></li>
    <li><a href="shop.php?format=vinyl" <?= $title == 'Vinyl Shop' ? 'class="current_page"' : ''?> >Shop Vinyl</a></li>
    <li><a href="shop.php?format=cassette" <?= $title == 'Cassette Shop' ? 'class="current_page"' : ''?> >Shop Cassettes</a></li>
    <li><a href="shop.php?genre=drumandbass" <?= $title == 'Drum & Bass Shop' ? 'class="current_page"' : ''?> >Shop Drum &amp; Bass</a></li>
    <li><a href="shop.php?genre=breakcore" <?= $title == 'Breakcore Shop' ? 'class="current_page"' : ''?> >Shop Breakcore</a></li>
    <li><a href="shop.php?genre=dubstep" <?= $title == 'Dubstep Shop' ? 'class="current_page"' : ''?> >Shop Dubstep</a></li>
    <li><a href="shop.php?genre=idm" <?= $title == 'IDM Shop' ? 'class="current_page"' : ''?> >Shop IDM</a></li>
    <li><a href="contact.php" <?= $title == 'Contact Us' ? 'class="current_page"' : ''?> >Contact Us</a></li>
    <li><a href="faq.php" <?= $title == 'F.A.Q.' ? 'class="current_page"' : ''?> >F.A.Q.</a></li>
  </ul>
</div>
