<?php

class Form {
  private $action;
  private $method;

  public function __construct($action = "#", $method = "POST") {
    $this->action = $action;
    $this->method = $method;
  }

  public function open() {
    return "<form action='{$this->action}' method='{$this->method}'>\n";
  }

  public function input($type = 'text', $name, $label) {
    $label = "<label for='{$name}'>{$label}: </label>";
    $input = "<input type='{$type}' name='{$name}' \>\n";

    return $label . $input;
  }

  public function submit($value) {
    return "<input type='submit' value='{$value}'";
  }

  public function select($name, $options = array()) {
    $options_out = "";
    foreach ($options as $key=>$value){
      $options_out .= "<option value='$key'>$value</option>\n";
    }
    $select = "<select name='$name'>\n";
    $select .= $options_out;
    $select .= "</select>";

    return $select;
  }

  public function close() {
    return "</form>";
  }
}
