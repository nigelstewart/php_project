<?php

ini_set('display_errors', 1);
ini_set('error_reporting', E_ALL);
define('TESTING', true);

define('DB_NAME', 'base16');
define('DB_USER', 'web_user');
define('DB_PASS', 'haverkretd');
define('DB_HOST', 'localhost');

// create connection to db
$dbh = new \PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS );
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// Begin sesssion and turn on output buffering
session_start();
ob_start();
