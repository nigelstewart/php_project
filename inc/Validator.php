<?php

/**
 * Description of validator
 *
 * @author nigel
 */

class Validator {
    // Create private members for errors and clean
    public $errors = [];
    private $clean = [];

    /*
     * Public validator funtion
     * @param String $rule the validation method to call
     * @param String $field_name field name to validate
     * @param String $value the value to validate
     */
    public function validate($rule, $field_name, $field_names = array(), $params = array()) {
        if(method_exists($this, $rule)) {
            $this->$rule($field_name, $field_names, $params);
        }
    }

    /*
     * Set error message function
     * @param String $field_name Field to set error for
     * @param String $message Error message to display
     */
    private function set_error($field_name, $message) {
        if(!isset($this->errors[$field_name])) {
                $this->errors[$field_name] = $message;
            }
    }

    /*
     * Required fields validation
     * @param String $field_name The required field's name
     */
    private function required($field_name) {
        if(!isset($_POST[$field_name]) || empty($_POST[$field_name])){
            $formatted = str_replace('_', ' ', $field_name);
            $formatted = ucwords($formatted);
            $this->set_error($field_name, "&#42;$formatted is a required field.");
        }
      $this->set_clean($field_name);
    }

    /*
     * Name string validation
     * @param String $field_name form field to be validated
    */
    private function name_string($field_name) {
      $pattern = "/^[ \x{00C0}-\x{01FF}a-zA-Z'\-]+$/u";
      $test = preg_match($pattern, $_POST[$field_name], $matches);

      if(!isset($matches[0]) || $matches[0] !== $_POST[$field_name]){
        $this->set_error($field_name, '&#42;Your name contains illegal characters.');
      }
      $this->set_clean($field_name);
    }

    /*
     * General string validation
     * @param String $field_name form field to be validated
    */
    private function string($field_name) {
      $pattern = "/^[ \x{00C0}-\x{01FF}a-zA-Z0-9'\-\.\(\)\#]+$/u";
      $test = preg_match($pattern, $_POST[$field_name], $matches);

      if(!isset($matches[0]) || $matches[0] !== $_POST[$field_name]){
        $formatted = str_replace('_', ' ', $field_name);
        $this->set_error($field_name, "&#42;Your $formatted contains illegal characters.");
      }
      $this->set_clean($field_name);
    }

    /*
     *  Method for validating email addresses
     * @param String $field_name form field to be validated
     * @param String $email email address to be validated
     */
    private function email($field_name){
        // Validate email using filter var
        if(!filter_var($_POST[$field_name], FILTER_VALIDATE_EMAIL)){
            $this->set_error($field_name, '&#42;Please enter a valid email address');
        }
      $this->set_clean($field_name);
    }

    /*
     * Method for validating postal codes
     * @param String $field_name form field to be validated
     * @param String $postal_code postal code to be validated
     */
    private function postalCode($field_name){
      // Validate postal code with regex
      $pattern = '/^[A-z]\d[A-z][ -]?\d[A-z]\d$/';
      $test = preg_match($pattern, $_POST[$field_name], $matches);

      if(!isset($matches[0]) || $matches[0] !== $_POST[$field_name]){
        $this->set_error($field_name, '&#42;You have entered an invalid postal code.');
      }
      $this->set_clean($field_name);
    }

    /*
    * Method for validating phone numbers
    * @param String $field_name form field to be validated
    */
    private function phoneNumber($field_name) {
      $pattern = "/[(]*([0-9]{3})[)]*-?\s?\.?([0-9]{3})-?\s?\.?([0-9]{4})/";
      $test = preg_match($pattern, $_POST[$field_name], $matches);

      if(!isset($matches[0]) || $matches[0] !== $_POST[$field_name]){
        $this->set_error($field_name, '&#42;You have entered an invalid phone number.');
      }
      $this->set_clean($field_name);
    }

    /*
    * Method for validating password
    * Must contain uppercase letter, number, and special character
    * @param String $field_name form field to be validated
    */
    private function passwordValidate($field_name) {
      $pattern = "/(?=.*\d+)(?=.*[A-Z]+)(?=.*[\!\@\#\$\%\^\&\*\.]+).{8,}/";
      $test = preg_match($pattern, $_POST[$field_name], $matches);

      if(!isset($matches[0]) || $matches[0] !== $_POST[$field_name]){
        $this->set_error($field_name, '&#42;Your password must be at least 8 characters long, and contain a number, uppercase letter, and one of the following special characters: ! @ # $ % ^ & * .');
      }
      $this->set_clean($field_name);
    }

    /*
    *Method for confirming matched passwords
    * @param String $field_name the confirm password field
    * @array Array $field_names both of the password fields
    */
    private function matchPassword($field_name, $field_names = array()) {
      if($field_names[0] !== $field_names[1]) {
        $this->set_error($field_name, 'Your passwords did not match.');
      }
      $this->set_clean($field_name);
    }

    /*
     *
     * @return Array errors array
     */
    public function errors($field_name = null) {
        if(is_null($field_name)){
            return $this->errors;
        }

        else {
            // check to make sure field name is set in $this->errors
            if(isset($this->errors[$field_name])) {
                // return the error
                return $this->errors[$field_name];
            }

            else {
                // return false
                return false;
            }
        }
    }

    private function len($field_name, $params = array()){
        if(isset($params['min'])){
            $min = $params['min'];
        } else {
            $min = 1; // Default length
        }

        if(isset($params['max'])){
            $max = $params['max'];
        } else {
            $max = 255; // Default length
        }

        if(strlen($_POST[$field_name]) < $min){
            $label = str_replace('_', ' ', $field_name);
            $label = ucwords($label);
            $message = "&#42;$label must have a minimum length of $min characters.";
            $this->set_error($field_name, $message);
        }

        if(strlen($_POST[$field_name]) > $max){
            $label = str_replace('_', ' ', $field_name);
            $label = ucwords($label);
            $message = "&#42;$label must have a maximum length of $max characters.";
            $this->set_error($field_name, $message);
        }
      $this->set_clean($field_name);
    }

    /*
     *
     * @param type $field_name
     */
    private function set_clean($field_name) {
        $value = strip_tags($_POST[$field_name]);
        $value = htmlentities($value);
        $this->clean[$field_name] = $value;
    }


    /*
     *
     * @return Array error array
     */
    public function clean($field_name = null) {

        if(is_null($field_name)) {
            return $this->clean;
        } else {

            // Check to make sure $field_name is set in $this->clean
            if(isset($this->clean[$field_name])) {

                return $this->clean[$field_name];

            } else {return false; }
        }
    }

}
