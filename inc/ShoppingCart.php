<?php

class ShoppingCart {
  public $cart = array();

  function __construct() {
    if (!isset($_SESSION['cart'])) {
      $_SESSION['cart'] = array();
    }
    $this->cart = $_SESSION['cart'];
  }

  public function isEmpty() {
    return empty($this->cart);
  }

  public function getItemQty() {
    return array_sum($this->cart);
  }

  public function addItem($product_id, $quantity) {
    $this->cart += array(
      $product_id => $quantity
    );
    $_SESSION['cart'] = $this->cart;
  }

  public function updateItem($product_id, $quantity) {
    if ($quantity === 0) {
      $this->removeItem($product_id);
    } elseif ($quantity > 0 && $quantity !== $this->cart[$product_id]) {
      $this->cart[$product_id] = $quantity;
    }
    $_SESSION['cart'] = $this->cart;
  }

  public function removeItem($product_id) {
    if (isset($this->cart[$product_id])) {
      unset($this->cart[$product_id]);
    }
    $_SESSION['cart'] = $this->cart;
  }

  public function emptyCart() {
    $this->cart = array();
    $_SESSION['cart'] = $this->cart;
  }
}


// // TESTING
// echo '<pre>';
// $my_cart = new ShoppingCart();
// echo $my_cart->getItems($dbh);
// echo 'Is cart empty? ';
// var_dump($my_cart->isEmpty());
// if ($my_cart->isEmpty()) {
//   echo 'Yes';
// } else { echo 'No'; }
//
// echo '<br />';
//
// $my_cart->addItem(1,1);
// $my_cart->addItem(3,1);
// print_r($my_cart->cart);
// $my_cart->removeItem(1);
// print_r($my_cart->cart);
//
// echo '<br />';
// echo 'Is cart empty? ';
// var_dump($my_cart->isEmpty());
// echo '<br />';
// $my_cart->addItem(1,1);
// $my_cart->updateItem(3,2);
// print_r($my_cart->cart);
// print_r($_SESSION['cart']);
// echo 'There are ' . $my_cart->getItemQty() . ' items in the cart.';
echo '</pre>';
