<?php

// Make sure required fields are filled
// function check_required($required, $errors){
//   foreach($required as $field_name) {
//     if(!isset($_POST[$field_name]) || empty($_POST[$field_name])){
//       $label = str_replace('_', ' ', $field_name);
//       $label = ucwords($label);
//       $errors[$field_name] = "$label is a required field";
//     }
//   }
//   return $errors;
// }


// Make province selects sticky
function get_sticky_values($provinces){
  foreach($_POST as $key => $value){
    if (isset($_POST[$key]) && !empty($_POST[$key])) {
      if (!is_array($value)){
        $value = strip_tags($value);
        $value = trim($value);
        $value = htmlentities($value);
      }
    }
    $selects[$key] = $value;
  }
  return $selects;
}

// Escape with HTML entities
function _esc($string) {
    echo htmlentities($string);
}

// Clean tags
function _esc_html($string) {
    $allowed = '<br><br /><p><strong><em>';
    echo strip_tags($string, $allowed);
}

// Validate
function validate_registration_form(Validator $v){
  // Instantiate validator as $v on required fields
  $v->validate('required', 'first_name');
  $v->validate('required', 'last_name');
  $v->validate('required', 'address');
  $v->validate('required', 'city');
  $v->validate('required', 'province');
  $v->validate('required', 'postal_code');
  $v->validate('required', 'country');
  $v->validate('required', 'phone');
  $v->validate('required', 'email');
  $v->validate('required', 'password');
  $v->validate('required', 'confirm_password');

  // Check lengths
  $v->validate('len', 'first_name', array('min' => 2, 'max' => 30));
  $v->validate('len', 'last_name', array('min' => 2, 'max' => 30));
  $v->validate('len', 'address', array());
  $v->validate('len', 'city', array('max' => 32));

  // String validation
  $v->validate('name_string', 'first_name');
  $v->validate('name_string', 'last_name');
  $v->validate('string', 'address');
  $v->validate('string', 'city');

  // Other Validation
  $v->validate('email', 'email', $_POST['email']);
  $v->validate('postalCode', 'postal_code', $_POST['postal_code']);
  $v->validate('phoneNumber', 'phone');
  $v->validate('passwordValidate', 'password');
  $v->validate('matchPassword', 'confirm_password', array($_POST['password'], $_POST['confirm_password']));
}

// Submit form
// If we have no missing info and everything is valid, submit data to DB
function submitForm() {
  // Create connection to DB
  $dbh = new PDO(
    'mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS
  );

  $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  // Create query
  $query = "INSERT INTO customer (first_name,
                                  last_name,
                                  address,
                                  city,
                                  postal_code,
                                  province,
                                  phone,
                                  email,
                                  password)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);";

  // Create parameters array
   $params = array(
       $_POST['first_name'],
       $_POST['last_name'],
       $_POST['address'],
       $_POST['city'],
       $_POST['postal_code'],
       $_POST['province'],
       $_POST['phone'],
       $_POST['email'],
       password_hash($_POST['password'], PASSWORD_DEFAULT)
   );

  // Prepare query
  $stmt = $dbh->prepare($query);

  // Execute query
  $stmt->execute($params);

  // Get customer_id
  $customer_id = $dbh->lastInsertId();

  // Store id in a cookie
  setcookie("cust_id", $customer_id);

  // Go to form submitted page
  header('Location: form_submitted.php');
}

// Shopping Cart functions
function getQty(ShoppingCart $my_cart){
  if ($my_cart->isEmpty()){
    echo 'Empty';
  } else {
    echo $my_cart->getItemQty();
  }
}
