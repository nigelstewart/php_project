<!DOCTYPE html>

<html>

  <head>
    <title><?= $title ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="styles/styles.css">
    <link rel="stylesheet" type="text/css" href="styles/mobile_styles.css">
    <link href="https://fonts.googleapis.com/css?family=Cousine:400,700" rel="stylesheet">
    <script src="https://use.fontawesome.com/6e7da69a63.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
    <script src="scripts/menu.js"></script>
  </head>

  <body>
    <header>
      <h1><a href="index.php">BASE16 <span style="color:#66FF99;">/</span><span style="color:#0099FF">/ </span><span style="font-family: Voyager-Grotesque;">RECORDS</span></a></h1>
      <img src="img/menu-down-arrow.png" id="menu_arrow" class="unrotated" width="40" height="40" alt="menu arrow"/>
      <?php if(isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === true) : ?>
        <div id="logged_in_user">
          <p>
            Welcome back, <strong><?= $_SESSION['user_name'] ?></strong><br />
            <a href="profile.php">Your Profile</a> &nbsp;
            <a href="login.php?logout=true">Log out</a>
          </p>
        </div>
      <?php else : ?>
        <p id="user_login"><a href="sign_up.php">Sign Up</a><a href="login.php">Log In</a></p>
      <?php endif; ?>
   </header>
