<?php
// include database and object files
include_once 'config/Database.php';
include_once 'objects/Product.php';

// get database connection
$database = new Database();
$dbh = $database->getConnection();

// prepare product object
$product = new Product($dbh);

// get id of product to be edited
$data = json_decode(file_get_contents("php://input"));

// set product property values
$product->sku = $data->sku;
$product->album_title = $data->album_title;
$product->artist = $data->artist;
$product->release_date = $data->release_date;
$product->release_country = $data->release_country;
$product->genre = $data->genre;
$product->label = $data->label;
$product->independent = $data->independent;
$product->format = $data->format;
$product->description = $data->description;
$product->price = $data->price;
$product->in_stock = $data->in_stock;
$product->album_cover = $data->album_cover;

// update the product
if($product->create()){
    echo "Product was added.";
}

// if unable to update the product, tell the user
else{
    echo "Unable to add product.";
}
?>
