<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object files
include_once 'config/Database.php';
include_once 'objects/Product.php';

// instantiate database and product object
$database = new Database();
$dbh = $database->getConnection();

// initialize object
$product = new Product($dbh);

// query products
$stmt = $product->readAll();
$num = $stmt->rowCount();

$data="";

// check if more than 0 record found
if($num>0){

    $x=1;

    // retrieve our table contents
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        extract($row);

        $data .= '{';
            $data .= '"product_id":"'  . $product_id . '",';
            $data .= '"sku":"'  . $sku . '",';
            $data .= '"title":"' . str_replace('"', '\"', $album_title) . '",';
            $data .= '"artist":"' . $artist . '",';
            $data .= '"release_date":"' . $release_date . '",';
            $data .= '"release_country":"' . $country_released . '",';
            $data .= '"genre":"' . $genre . '",';
            $data .= '"label":"' . $label . '",';
            $data .= '"independent":"' . $independent . '",';
            $data .= '"format":"' . $format . '",';
            $data .= '"description":"' . str_replace('"', '\"', $description) . '",';
            $data .= '"price":"' . $price . '",';
            $data .= '"in_stock":"' . $in_stock . '",';
            $data .= '"album_cover":"' . $cover_img . '"';
        $data .= '}';

        $data .= $x<$num ? ',' : ''; $x++; }
}

// json format output
echo '{"records":[' . $data . ']}';
?>
