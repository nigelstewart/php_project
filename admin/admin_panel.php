<?php

require_once '../inc/config.php';

if (($_SESSION['logged_in'] === true) && ($_SESSION['is_admin'] === true)) {

  $title = 'Admin Panel';

} else {
  header('Location: login.php?logout=true');
  exit;
}

?><!DOCTYPE html>
<html>
  <head>
    <title><?=$title?></title>
    <meta charset="utf-8">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Istok+Web:400,700" rel="stylesheet">
    <script src="https://use.fontawesome.com/6e7da69a63.js"></script>
    <link rel="stylesheet" type="text/css" href="styles/admin_style.css">
  </head>

  <body>
    <header id="panel_header">
      <h1 id="admin_panel_header"><?=$title?></h1>
      <a href="login.php?logout=true"><i class="fa fa-sign-out" aria-hidden="true"></i> Log Out</a>
      <a href="admin_panel.php"><i class="fa fa-product-hunt" aria-hidden="true"></i> Manage Products</a>
      <a href="stats.php"><i class="fa fa-bar-chart" aria-hidden="true"></i> Statistics</a>
    </header>

    <div id="panel_content" ng-app="myApp" ng-controller="productsCtrl">
      <div id="utilities">
        <input type="text" ng-model="search" placeholder="Filter products"/>
        <a id="add_product">New Product <i class="fa fa-plus-square" aria-hidden="true"></i></a>
      </div>

      <div id="new_product">
        <table class="product">
          <thead>
            <tr>
              <th>SKU</th>
              <th>Title</th>
              <th>Artist</th>
              <th>Release Date</th>
              <th>Release Country</th>
              <th>Genre</th>
              <th>Label</th>
              <th>Independent</th>
              <th>Format</th>
              <th>Description</th>
              <th>Price</th>
              <th>In Stock</th>
              <th>Album Cover</th>
              <th class="save">Save</th>
              <th class="del">Cancel</th>
            </tr>
          </thead>
          <tbody>
            <td><input type="text" ng-model="new_product.sku" /></td>
            <td><input type="text" ng-model="new_product.album_title" /></td>
            <td><input type="text" ng-model="new_product.artist" /></td>
            <td><input type="text" ng-model="new_product.release_date" /></td>
            <td><input type="text" ng-model="new_product.release_country" /></td>
            <td><input type="text" ng-model="new_product.genre" /></td>
            <td><input type="text" ng-model="new_product.label" /></td>
            <td><input type="text" ng-model="new_product.independent" /></td>
            <td><input type="text" ng-model="new_product.format" /></td>
            <td><input type="text" ng-model="new_product.description" /></td>
            <td><input type="text" ng-model="new_product.price" /></td>
            <td><input type="text" ng-model="new_product.in_stock" /></td>
            <td>
              <label class="image-upload">
                <i class="fa fa-upload" aria-hidden="true"></i> <input type="file" ng-model="new_product.album_cover"/>
              </label>
            </td>
            <td>
              <a id="save_new" class="floppy" ng-click="addProduct(new_product.sku,
                                                     new_product.album_title,
                                                     new_product.artist,
                                                     new_product.release_date,
                                                     new_product.release_country,
                                                     new_product.genre,
                                                     new_product.label,
                                                     new_product.independent,
                                                     new_product.format,
                                                     new_product.description,
                                                     new_product.price,
                                                     new_product.in_stock,
                                                     new_product.album_cover)">
              <i class="fa fa-floppy-o" aria-hidden="true"></i></a></td>
            <td>
              <a id="cancel" ng-click="clearForm()">
              <i class="fa fa-times-circle-o" aria-hidden="true"></i></a>
            </td>
          </tbody>
        </table>
      </div>

      <table class="product">
        <thead>
          <tr>
            <th class="id">ID</th>
            <th>SKU</th>
            <th>Title</th>
            <th>Artist</th>
            <th>Release Date</th>
            <th>Release Country</th>
            <th>Genre</th>
            <th>Label</th>
            <th>Independent</th>
            <th>Format</th>
            <th>Description</th>
            <th>Price</th>
            <th>In Stock</th>
            <th>Album Cover</th>
            <th class="save">Save</th>
            <th class="del">Del</th>
          </tr>
        </thead>
        <tbody ng-init="getAll()">
          <tr class="id" ng-repeat="product in products | filter:search">
            <td>{{ product.product_id }}</td>
            <td><input type="text" ng-model="product.sku" /></td>
            <td><input type="text" ng-model="product.title" /></td>
            <td><input type="text" ng-model="product.artist" /></td>
            <td><input type="text" ng-model="product.release_date" /></td>
            <td><input type="text" ng-model="product.release_country" /></td>
            <td><input type="text" ng-model="product.genre" /></td>
            <td><input type="text" ng-model="product.label" /></td>
            <td><input type="text" ng-model="product.independent" /></td>
            <td><input type="text" ng-model="product.format" /></td>
            <td><input type="text" ng-model="product.description" /></td>
            <td><input type="text" ng-model="product.price" /></td>
            <td><input type="text" ng-model="product.in_stock" /></td>
            <td>
              <label class="image-upload">
                <img src="../img/album_cover/{{ product.album_cover }}" width="60" height="60"/>
                <input type="file" />
              </label>
            </td>
            <td>
              <a class="floppy" ng-click="updateProduct(product.product_id,
                                                        product.sku,
                                                        product.title,
                                                        product.artist,
                                                        product.release_date,
                                                        product.release_country,
                                                        product.genre,
                                                        product.label,
                                                        product.independent,
                                                        product.format,
                                                        product.description,
                                                        product.price,
                                                        product.in_stock,
                                                        product.album_cover)">
              <i class="fa fa-floppy-o" aria-hidden="true"></i></a></td>
            <td>
              <a class="trash" ng-click="deleteProduct(product.product_id)">
              <i class="fa fa-trash" aria-hidden="true"></i></a>
            </td>
            <!-- <td rowspan="4">
              <img src="../img/album_cover/{{ product.album_cover }}" width="124" height="124" alt="cover" />
            </td> -->
          </tr>
        </tbody>
      </table>

    </div>

    <script src="libs/angular-1.5.8/angular.js"></script>
    <script>
      var app = angular.module('myApp', []);
      app.controller('productsCtrl', function($scope, $http) {

        // read products
        $scope.getAll = function(){
          $http.get("read_products.php").success(function(response){
            $scope.products = response.records;
          });
        }

        // clear new product
        $scope.clearForm = function() {
          $scope.new_product.sku = "";
          $scope.new_product.album_title = "";
          $scope.new_product.artist = "";
          $scope.new_product.release_date = "";
          $scope.new_product.release_country = "";
          $scope.new_product.genre = "";
          $scope.new_product.label = "";
          $scope.new_product.independent = "";
          $scope.new_product.format = "";
          $scope.new_product.description = "";
          $scope.new_product.price = "";
          $scope.new_product.in_stock = "";
          $scope.new_product.album_cover = "";
        }

        // add product
        $scope.addProduct = function(sku, album_title, artist, release_date, release_country, genre, label, independent, format, description, price, in_stock, album_cover) {
          $http.post('create_product.php', {
            'sku' : sku,
            'album_title' : album_title,
            'artist' : artist,
            'release_date' : release_date,
            'release_country' : release_country,
            'genre' : genre,
            'label' : label,
            'independent' : independent,
            'format' : format,
            'description' : description,
            'price' : price,
            'in_stock' : in_stock,
            'album_cover' : album_cover
          })
          .success(function (data, status, headers, config){
            alert('Product was added.');
            $scope.clearForm();
            $scope.getAll();
          });
        }

        // update product record / save changes
        $scope.updateProduct = function(product_id, sku, album_title, artist, release_date, release_country, genre, label, independent, format, description, price, in_stock, album_cover) {
            $http.post('update_product.php', {
                'product_id' : product_id,
                'sku' : sku,
                'album_title' : album_title,
                'artist' : artist,
                'release_date' : release_date,
                'release_country' : release_country,
                'genre' : genre,
                'label' : label,
                'independent' : independent,
                'format' : format,
                'description' : description,
                'price' : price,
                'in_stock' : in_stock,
                'album_cover' : album_cover
            })
            .success(function (data, status, headers, config){
                // refresh the product list
                alert('Product was updated.');
                $scope.getAll();
            });
        }

        // delete product
        $scope.deleteProduct = function(product_id){
          // ask the user if he is sure to delete the record
          if(confirm("Are you sure?")){
            // post the id of product to be deleted
            $http.post('delete_product.php', {
            'product_id' : product_id
            }).success(function (data, status, headers, config){
              // refresh the list
              alert('Product was deleted.');
              $scope.getAll();
            });
          }
        }

      });

      $( "#add_product" ).click(function() {
        $( "#new_product" ).slideDown( "slow", function() {
          // Animation complete.
        });
      });

      $( "#cancel" ).click(function() {
        $( "#new_product" ).slideUp( "slow", function() {
          // Animation complete.
        });
      });

      $( "#save_new" ).click(function() {
        $( "#new_product" ).slideUp( "slow", function() {
          // Animation complete.
        });
      });
    </script>
  </body>
</html>
