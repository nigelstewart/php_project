<?php

require_once '../inc/config.php';

// order avg totalItems

// PRODUCTS FUNCTIONS

function getTotalNumItems($dbh) {
  // Create query
  $query = "SELECT COUNT(*) FROM product";

  // Prepare query
  $stmt = $dbh->prepare($query);

  // Execute query
  $stmt->execute();

  //Fetch results
  $total = $stmt->fetch(PDO::FETCH_ASSOC);
  return $total;
}

function getNumItemsByGenre($dbh) {
  // Create query
  $query = "SELECT genre, COUNT(*) FROM product GROUP BY genre";

  // Prepare query
  $stmt = $dbh->prepare($query);

  // Execute query
  $stmt->execute();

  // Fetch results
  $nums = $stmt->fetchAll(PDO::FETCH_ASSOC);
  return $nums;
}

function getNumItemsByFormat($dbh) {
  // Create query
  $query = "SELECT format, COUNT(*) FROM product GROUP BY format";

  // Prepare query
  $stmt = $dbh->prepare($query);

  // Execute query
  $stmt->execute();

  // Fetch results
  $nums = $stmt->fetchAll(PDO::FETCH_ASSOC);
  return $nums;
}

function maxPrice($dbh) {
  // Create query
  $query = "SELECT MAX(price) FROM product";

  // Prepare query
  $stmt = $dbh->prepare($query);

  // Execute query
  $stmt->execute();

  // Fetch results
  $max_price = $stmt->fetch(PDO::FETCH_ASSOC);
  return $max_price;
}

function minPrice($dbh) {
  // Create query
  $query = "SELECT MIN(price) FROM product";

  // Prepare query
  $stmt = $dbh->prepare($query);

  // Execute query
  $stmt->execute();

  // Fetch results
  $min_price = $stmt->fetch(PDO::FETCH_ASSOC);
  return $min_price;
}

function avgPrice($dbh) {
  // Create query
  $query = "SELECT AVG(price) FROM product";

  // Prepare query
  $stmt = $dbh->prepare($query);

  // Execute query
  $stmt->execute();

  // Fetch results
  $avg_price = $stmt->fetch(PDO::FETCH_ASSOC);
  return $avg_price;
}

// ORDERS FUNCTIONS

function totalNumOrders($dbh) {
  // Create query
  $query = "SELECT COUNT(*) FROM invoice";

  // Prepare query
  $stmt = $dbh->prepare($query);

  // Execute query
  $stmt->execute();

  //Fetch results
  $orders = $stmt->fetch(PDO::FETCH_ASSOC);
  return $orders;
}

function maxOrder($dbh) {
  // Create query
  $query = "SELECT MAX(total_cost) FROM invoice";

  // Prepare query
  $stmt = $dbh->prepare($query);

  // Execute query
  $stmt->execute();

  // Fetch results
  $max_order = $stmt->fetch(PDO::FETCH_ASSOC);
  return $max_order;
}

function minOrder($dbh) {
  // Create query
  $query = "SELECT MIN(total_cost) FROM invoice";

  // Prepare query
  $stmt = $dbh->prepare($query);

  // Execute query
  $stmt->execute();

  // Fetch results
  $min_order = $stmt->fetch(PDO::FETCH_ASSOC);
  return $min_order;
}


function avgOrder($dbh) {
  // Create query
  $query = "SELECT AVG(total_cost) FROM invoice";

  // Prepare query
  $stmt = $dbh->prepare($query);

  // Execute query
  $stmt->execute();

  // Fetch results
  $avg_total = $stmt->fetch(PDO::FETCH_ASSOC);
  return $avg_total;
}

function itemsPerOrder($dbh) {

}

// CUSTOMER FUNCTIONS
function numberOfCustomers($dbh) {
  // Create query
  $query = "SELECT COUNT(*) FROM customer";

  // Prepare query
  $stmt = $dbh->prepare($query);

  // Execute query
  $stmt->execute();

  //Fetch results
  $customers = $stmt->fetch(PDO::FETCH_ASSOC);
  return $customers;
}

function lastCustomer($dbh) {
  // Create query
  $query = "SELECT customer_id, first_name, last_name
            FROM invoice
            ORDER BY invoice_id DESC LIMIT 1";

  // Prepare query
  $stmt = $dbh->prepare($query);

  // Execute query
  $stmt->execute();

  //Fetch results
  $last_customer = $stmt->fetch(PDO::FETCH_ASSOC);
  return $last_customer;
}
