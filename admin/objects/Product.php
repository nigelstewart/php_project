<?php

  class Product {
    private $conn;
    private $table_name = "product";

    // Object properties
    public $product_id;
    public $sku;
    public $title;
    public $artist;
    public $release_date;
    public $release_country;
    public $genre;
    public $label;
    public $independent;
    public $format;
    public $description;
    public $price;
    public $in_stock;
    public $album_cover;

    public function __construct($dbh) {
      $this->conn = $dbh;
    }

    public function readAll() {
      // Create query
      $query = "SELECT product_id,
                       sku,
                       album_title,
                       artist,
                       release_date,
                       country_released,
                       genre,
                       label,
                       independent,
                       format,
                       description,
                       price,
                       in_stock,
                       cover_img
              FROM product
              WHERE deleted=0
              ORDER BY product_id ASC";

      // Prepare query
      $stmt = $this->conn->prepare( $query );

      // Execute query
      $stmt->execute();

      return $stmt;
    }

    public function create(){
      // Create query
      $query = "INSERT INTO product
                SET sku = :sku,
                    album_title = :album_title,
                    artist = :artist,
                    release_date = :release_date,
                    country_released = :release_country,
                    genre = :genre,
                    label = :label,
                    independent = :independent,
                    format = :format,
                    description = :description,
                    price = :price,
                    in_stock = :in_stock,
                    cover_img = :album_cover";

      // Prepare query
      $stmt = $this->conn->prepare($query);

      // Clean values
      $this->sku=htmlspecialchars(strip_tags($this->sku));
      $this->album_title=(strip_tags($this->album_title));
      $this->artist=htmlspecialchars(strip_tags($this->artist));
      $this->release_date=htmlspecialchars(strip_tags($this->release_date));
      $this->release_country=htmlspecialchars(strip_tags($this->release_country));
      $this->genre=htmlspecialchars(strip_tags($this->genre));
      $this->label=htmlspecialchars(strip_tags($this->label));
      $this->independent=htmlspecialchars(strip_tags($this->independent));
      $this->format=htmlspecialchars(strip_tags($this->format));
      $this->description=(strip_tags($this->description));
      $this->price=htmlspecialchars(strip_tags($this->price));
      $this->in_stock=htmlspecialchars(strip_tags($this->in_stock));
      $this->album_cover=htmlspecialchars(strip_tags($this->album_cover));

      // Bind values
      $stmt->bindParam(':sku', $this->sku);
      $stmt->bindParam(':album_title', $this->album_title);
      $stmt->bindParam(':artist', $this->artist);
      $stmt->bindParam(':release_date', $this->release_date);
      $stmt->bindParam(':release_country', $this->release_country);
      $stmt->bindParam(':genre', $this->genre);
      $stmt->bindParam(':label', $this->label);
      $stmt->bindParam(':independent', $this->independent);
      $stmt->bindParam(':format', $this->format);
      $stmt->bindParam(':description', $this->description);
      $stmt->bindParam(':price', $this->price);
      $stmt->bindParam(':in_stock', $this->in_stock);
      $stmt->bindParam(':album_cover', $this->album_cover);

      if($stmt->execute()){
        return true;
      } else {
        return false;
      }
    }

    public function update(){
      // Create query
      $query = "UPDATE product
                SET sku = :sku,
                    album_title = :album_title,
                    artist = :artist,
                    release_date = :release_date,
                    country_released = :release_country,
                    genre = :genre,
                    label = :label,
                    independent = :independent,
                    format = :format,
                    description = :description,
                    price = :price,
                    in_stock = :in_stock,
                    cover_img = :album_cover
                WHERE product_id = :product_id";

      // Prepare query
      $stmt = $this->conn->prepare($query);

      // Clean values
      $this->sku=htmlspecialchars(strip_tags($this->sku));
      $this->album_title=(strip_tags($this->album_title));
      $this->artist=htmlspecialchars(strip_tags($this->artist));
      $this->release_date=htmlspecialchars(strip_tags($this->release_date));
      $this->release_country=htmlspecialchars(strip_tags($this->release_country));
      $this->genre=htmlspecialchars(strip_tags($this->genre));
      $this->label=htmlspecialchars(strip_tags($this->label));
      $this->independent=htmlspecialchars(strip_tags($this->independent));
      $this->format=htmlspecialchars(strip_tags($this->format));
      $this->description=(strip_tags($this->description));
      $this->price=htmlspecialchars(strip_tags($this->price));
      $this->in_stock=htmlspecialchars(strip_tags($this->in_stock));
      $this->album_cover=htmlspecialchars(strip_tags($this->album_cover));

      // Bind values
      $stmt->bindParam(':sku', $this->sku);
      $stmt->bindParam(':album_title', $this->album_title);
      $stmt->bindParam(':artist', $this->artist);
      $stmt->bindParam(':release_date', $this->release_date);
      $stmt->bindParam(':release_country', $this->release_country);
      $stmt->bindParam(':genre', $this->genre);
      $stmt->bindParam(':label', $this->label);
      $stmt->bindParam(':independent', $this->independent);
      $stmt->bindParam(':format', $this->format);
      $stmt->bindParam(':description', $this->description);
      $stmt->bindParam(':price', $this->price);
      $stmt->bindParam(':in_stock', $this->in_stock);
      $stmt->bindParam(':album_cover', $this->album_cover);
      $stmt->bindParam(':product_id', $this->product_id);

      if($stmt->execute()){
        return true;
      } else {
        return false;
      }
    }

    public function delete() {
      // Create query
      $query = "UPDATE product
                SET deleted=1
                WHERE product_id = ?";

      // Prepare query
      $stmt = $this->conn->prepare($query);

      // Bind id of record to delete
      $stmt->bindParam(1, $this->product_id);

      // Execute query
      if($stmt->execute()) {
        return true;
      } else {
        return false;
      }
    }
  }

// $products = new Product();
// var_dump($products->readAll($dbh));
