<?php

require_once '../inc/config.php';
require_once 'aggregate_functions.php';

if (($_SESSION['logged_in'] === true) && ($_SESSION['is_admin'] === true)) {

  $title = 'Admin Panel';

  $num_items_by_genre = getNumItemsByGenre($dbh);
  $num_items_by_format = getNumItemsByFormat($dbh);
  $totalItems = getTotalNumItems($dbh);
  $average_price = avgPrice($dbh);
  $max_price = maxPrice($dbh);
  $min_price = minPrice($dbh);
  $max_order = maxOrder($dbh);
  $min_order = minOrder($dbh);
  $average_order_total = avgOrder($dbh);
  $number_of_orders = totalNumOrders($dbh);
  $number_of_customers = numberOfCustomers($dbh);
  $last_customer = lastCustomer($dbh);

} else {
  header('Location: login.php?logout=true');
  exit;
}



?><!DOCTYPE html>
<html>
  <head>
    <title><?=$title?></title>
    <meta charset="utf-8">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Istok+Web:400,700" rel="stylesheet">
    <script src="https://use.fontawesome.com/6e7da69a63.js"></script>
    <link rel="stylesheet" type="text/css" href="styles/admin_style.css">
  </head>

  <body>
    <header id="panel_header">
      <h1 id="admin_panel_header"><?=$title?></h1>
      <a href="login.php?logout=true"><i class="fa fa-sign-out" aria-hidden="true"></i> Log Out</a>
      <a href="admin_panel.php"><i class="fa fa-product-hunt" aria-hidden="true"></i> Manage Products</a>
      <a href="stats.php"><i class="fa fa-bar-chart" aria-hidden="true"></i> Statistics</a>
    </header>

    <div id="stats_content">

      <div class="stat_group">
        <h2>Products</h2>
        <hr />
        <?php foreach ($num_items_by_format as $arrays) : ?>
          <p><?= $arrays['format'] ?>: <span class="value"><?= $arrays['COUNT(*)'] ?></span></p>
        <?php endforeach; ?>
        <?php foreach ($num_items_by_genre as $arrays) : ?>
          <p><?= $arrays['genre'] ?>: <span class="value"><?= $arrays['COUNT(*)'] ?></span></p>
        <?php endforeach; ?>
        <p>Total: <span class="value"><?=$totalItems['COUNT(*)']?></span></p>
        <p>Max Price: <span class="value">$<?=$max_price['MAX(price)']?></span></p>
        <p>Min Price: <span class="value">$<?=$min_price['MIN(price)']?></span></p>
        <p>
          Average Price:
          <span class="value">$<?=number_format($average_price['AVG(price)'], 2)?></span>
        </p>
      </div>

      <div class="stat_group">
        <h2>Orders</h2>
        <hr />
        <p>Orders placed: <span class="value"><?=$number_of_orders['COUNT(*)']?></span></p>
        <p>Max Total Per Order: <span class="value">$<?=$max_order['MAX(total_cost)']?></span></p>
        <p>Min Total Per Order: <span class="value">$<?=$min_order['MIN(total_cost)']?></span></p>
        <p>Average Total Per Order: <span class="value">$<?=number_format($average_order_total['AVG(total_cost)'], 2)?></span></p>
      </div>

      <div class="stat_group">
        <h2>Customers</h2>
        <hr />
        <p>Customers in Database: <span class="value"><?=$number_of_customers['COUNT(*)']?></span></p>
        <p>Last customer to order:
          <span class="value">
          <?=
            $last_customer['first_name'] . ' ' .
            $last_customer['last_name'] . ' ' .
            $last_customer['customer_id']
          ?>
        </span>
        </p>
    </div>
  </body>
</html>
