<?php

require_once '../inc/config.php';

$title = "Log in";

if (isset($_GET['logout'])) {
  $_SESSION['logged_in'] = false;
  $_SESSION['is_admin'] = false;
  session_regenerate_id();
  $login_msg = "Please log in to continue.";
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

  if (!isset($_POST['email']) || empty($_POST['email'])) {
    $login_msg = "Please enter your email.";
  } elseif (!isset($_POST['password']) || empty($_POST['password'])) {
    $login_msg = "You didn't enter a password.";
  }

  $user_email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
  $user_password = $_POST['password'];

  if(!isset($login_msg) && empty($login_msg)) {
    // Query database for user info
    $query = "SELECT *
              FROM customer
              WHERE email = (?)
              AND is_admin = 1";

    // Create params
    $params = array($user_email);

    // Prepare query
    $stmt = $dbh->prepare($query);

    //Check if execute query
    if($stmt->execute($params)){
      // Fetch user
      $user = $stmt->fetch(PDO::FETCH_ASSOC);
      if(!$user || !password_verify($user_password, $user['password'])) {
        $_SESSION['logged_in'] = false; // Set session logged_in to false
        $login_msg = "Invalid email/password combination.";
      } else {
        $_SESSION['logged_in'] = true; // Set session logged_in to true
        $_SESSION['is_admin'] = true;
        session_regenerate_id();
        header('Location: stats.php');
        exit;
      }
    } // End if execute query
  } // End if no login msg
} // End if POST
?><!DOCTYPE html>
<html>

  <head>
    <title>Admin Login</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="styles/admin_style.css">
    <link href="https://fonts.googleapis.com/css?family=Istok+Web:400,700" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
  </head>

  <body>
    <?php if (isset($login_msg)) : ?>
      <div id="login_messages">
        <?= $login_msg ?>
      </div>
    <?php endif; ?>
    <div id="container">
      <form id="login_form" action="login.php" method="POST">
        <p>
          <label for="email">Email: </label>
          <input type="text" name="email" value="<?php if(isset($user_email)){ echo $user_email; } ?>"/>
        </p>
        <p>
          <label for="password">Password: </label>
          <input type="password" name="password" />
        </p>
        <p id="login_button"><input type="submit" Value="Log In" /></p>
      </form>
    </div>

    <script>
        $(document).ready(function() {
            $("#login_messages").hide()
                    .slideDown()
                    .delay(2000)
                    .slideUp('slow')
        });
    </script>
  </body>

</html>
