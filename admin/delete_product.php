<?php
// include database and object file
include_once 'config/Database.php';
include_once 'objects/Product.php';

// get database connection
$database = new Database();
$dbh = $database->getConnection();

// prepare product object
$product = new Product($dbh);

// get product id
$data = json_decode(file_get_contents("php://input"));

// set product id to be deleted
$product->product_id = $data->product_id;

// delete the product
if($product->delete()){
    echo "Product was deleted.";
}

// if unable to delete the product
else{
    echo "Unable to delete object.";
}
?>
