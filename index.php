<?php

require_once 'inc/config.php';
require_once 'inc/functions.php';
require_once 'inc/ShoppingCart.php';

// Insantiate the shopping cart
$my_cart = new ShoppingCart();

$title = "Base 16 Records";

include('inc/header.php'); ?>

  <div id="wrapper">

    <!-- Menu Include -->
    <?php include('inc/menu.php'); ?>

    <div id="content">
      <img src="img/record.svg" alt="record"/>
      <p id="base_hex">42 / 61 / 73 / 65 / 31 / 36 / 20</p>
      <h2 id="about">
        Base 16 is a Canadian electronic music distro that specializes in
        drum & bass, breakcore, and beyond. <br />
        Bringing the best physical releases from around the world to your
        door step.
      </h2>
    </div>

  </div>

<?php include('inc/footer.php'); ?>
