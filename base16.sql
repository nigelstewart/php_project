DROP DATABASE IF EXISTS base16;
CREATE DATABASE base16
  CHARACTER SET utf8
  DEFAULT CHARACTER SET utf8
  COLLATE utf8_general_ci
  DEFAULT COLLATE utf8_general_ci;
GRANT all ON base16.* to 'web_user'@'localhost' identified by 'haverkretd';
USE base16;

--
-- Table structure for customer
--

DROP TABLE IF EXISTS customer;
CREATE TABLE customer (
  customer_id int(11) NOT NULL AUTO_INCREMENT,
  first_name varchar(255) NOT NULL,
  last_name varchar(255) NOT NULL,
  email varchar(255) NOT NULL,
  address varchar(255) NOT NULL,
  city varchar(255) NOT NULL,
  postal_code varchar(7) NOT NULL,
  province char(2) NOT NULL,
  country varchar(255) NOT NULL DEFAULT 'Canada',
  phone varchar(12) NOT NULL,
  password varchar(255) NOT NULL,
  is_admin bool NOT NULL DEFAULT false,
  created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at datetime DEFAULT NULL,
  deleted bool NOT NULL DEFAULT false,
  PRIMARY KEY (customer_id)
);

INSERT INTO customer VALUES (1,'Admin','User','admin@base16.com','99 Evergreen Terrace','Winnipeg','R2L 1X7','MB','Canada','204-999-9999','$2y$10$r55PDCX7ZrvdQE35GnXgo.CYNue4UB/bN6njXlr8YoHkXyRspyr.i',1,NOW(),NULL,0);
INSERT INTO customer VALUES (2,'Nigel','Stewart','nigelstewart@ftml.net','123 Fake Street','Winnipeg','R3J 2V9','MB','Canada','204-999-9999','$2y$10$r55PDCX7ZrvdQE35GnXgo.CYNue4UB/bN6njXlr8YoHkXyRspyr.i',0,NOW(),NULL,0);

--
-- Table structure for table invoice
--

DROP TABLE IF EXISTS invoice;
CREATE TABLE invoice (
  invoice_id int(11) NOT NULL AUTO_INCREMENT,
  customer_id int(11) NOT NULL,
  first_name varchar(255) NOT NULL,
  last_name varchar(255) NOT NULL,
  email varchar(255) NOT NULL,
  address varchar(255) NOT NULL,
  city varchar(255) NOT NULL,
  postal_code varchar(7) NOT NULL,
  province char(2) NOT NULL,
  country varchar(255) NOT NULL DEFAULT 'Canada',
  phone varchar(12) NOT NULL,
  card_digits char(4) NOT NULL,
  order_date datetime DEFAULT NULL,
  products_subtotal decimal(6,2) NOT NULL,
  tax decimal(6,2) NOT NULL,
  total_cost decimal(6,2) NOT NULL,
  created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at datetime DEFAULT NULL,
  deleted bool NOT NULL DEFAULT false,
  PRIMARY KEY (invoice_id)
);

--
-- Table structure for table invoice_products
--

DROP TABLE IF EXISTS invoice_products;
CREATE TABLE invoice_products (
  invoice_id int(11) NOT NULL,
  product_id int(11) NOT NULL,
  quantity int(11) NOT NULL,
  price_per_unit decimal(6,2) NOT NULL,
  line_total decimal(6,2) DEFAULT NULL,
  created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at datetime DEFAULT NULL,
  deleted bool NOT NULL DEFAULT false,
  PRIMARY KEY (invoice_id,product_id)
);

--
-- Table structure for table product
--

DROP TABLE IF EXISTS product;
CREATE TABLE product (
  product_id int(11) NOT NULL AUTO_INCREMENT,
  sku varchar(55) NOT NULL,
  album_title varchar(255) NOT NULL,
  artist varchar(255) NOT NULL,
  release_date date NOT NULL,
  country_released varchar(255) NOT NULL,
  genre varchar(255) NOT NULL,
  label varchar(255) DEFAULT NULL,
  independent tinyint(1) NOT NULL DEFAULT '1',
  format varchar(255) NOT NULL,
  description varchar(255) NOT NULL,
  price decimal(6,2) NOT NULL,
  in_stock tinyint(1) NOT NULL DEFAULT '1',
  cover_img varchar(255) NOT NULL,
  created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at datetime DEFAULT NULL,
  deleted bool NOT NULL DEFAULT false,
  PRIMARY KEY (product_id)
);

--
-- Dumping data for table product
--

INSERT INTO product
VALUES
(1,'ONEF001','Elephant Dreams EP','Alix Perez','2016-07-01','U.K.','Drum & Bass','1985 Music',0,'Vinyl','12", 33 1/3 RPM. 1 x Black Vinyl.',29.99,1,'elephant_dreams.jpg','2016-07-19 01:57:13','2016-07-19 01:57:13',0),
(2,'SUICIDELP014SAMPLER','The Draft','Klute','2014-07-27','U.K.','Drum & Bass','Commercial Suicide Records',0,'Vinyl','12", 33 1/3 RPM. 2 x Red Vinyl.',19.99,1,'the_draft_lp.jpg','2016-07-19 01:57:13','2016-07-19 01:57:13',0),
(3,'CRLP-001','Hellnova','Sans Soleil','2016-03-18','Germany','Drum & Bass','Confunktion Records',0,'Vinyl','12", 33 1/3 RPM. 2 x Black Vinyl.',39.99,1,'hellnova.jpg','2016-07-19 01:57:13','2016-07-19 01:57:13',0),
(4,'ISHCHAT001','Only You EP','Spectrasoul','2016-03-25','U.K.','Drum & Bass','Ish Chat Music',0,'Vinyl','12", 33 1/3 RPM. 1 x Black Vinyl.',18.00,1,'only_you.jpg','2016-07-19 01:57:13','2016-07-19 01:57:13',0),
(5,'TECHITCH01','Souls of Impatience EP','Technical Itch','2016-06-10','U.K.','Drum & Bass','Tech Itch Recordings',1,'Vinyl','12", 33 1/3 RPM. 1 x Black Vinyl.',18.00,1,'souls_of_impatience_ep.jpg','2016-07-19 01:57:13','2016-07-19 01:57:13',0),
(6,'NHS294TP','Oxygen','Fred V & Graffix','2016-06-17','U.K.','Drum & Bass','Hospital Records',0,'Vinyl','12", 33 1/3 RPM. 2 x Black Vinyl.',34.99,1,'oxygen.jpg','2016-07-19 01:57:13','2016-07-19 01:57:13',0),
(7,'NHS264T','Unrecognisable','Fred V & Graffix','2014-11-24','U.K.','Drum & Bass','Hospital Records',0,'Vinyl','12", 33 1/3 RPM. 1 x Black Vinyl.',12.00,1,'unrecognisable.jpg','2016-07-19 01:57:13','2016-07-19 01:57:13',0),
(8,'MINDTECH006','MINDTECH006','Allied & Nphonix','2015-04-13','Belgium','Drum & Bass','Mindtech Recordings',0,'Vinyl','12", 33 1/3 RPM. 1 x Black Vinyl.',14.99,1,'mindtech_006.jpg','2016-07-19 01:57:13','2016-07-19 01:57:13',0),
(9,'MOZYK014','Float','Umio  & Xyqph','2015-05-14','Bulgaria','Breakcore','Mozyk',0,'Cassette','Ltd Edition Cassette x 1.',12.00,1,'float.jpg','2016-07-19 01:57:13','2016-07-19 01:57:13',0),
(10,'MOZYK010','Fake Fur Seeds','Umio','2014-08-13','Bulgaria','Breakcore','Mozyk',0,'Cassette','Ltd Edition Cassette x 1.',12.00,1,'fake_fur_seeds.jpg','2016-07-19 01:57:13','2016-07-19 01:57:13',0),
(11,'ADN165LP','Hallelujah','Igorrr','2012-12-21','Germany','Breakcore','Ad noiseam',0,'Vinyl','12", 33 1/3 RPM. 1 x Clear Vinyl.',45.00,1,'hallelujah.jpg','2016-07-19 01:57:13','2016-07-19 01:57:13',0),
(12,'ADN166','Machine Throne','Gore Tech','2013-03-04','Germany','Breakcore','Ad noiseam',0,'Vinyl','12", 33 1/3 RPM. 1 x Black Vinyl.',14.99,1,'machine_throne.jpg','2016-07-19 01:57:13','2016-07-19 01:57:13',0),
(13,'MINDSET013','Spawned','Biome','2013-01-21','U.K.','Dubstep','Mindset Records',0,'Vinyl','12", 33 1/3 RPM. 1 x Black Vinyl.',9.99,1,'spawned.jpg','2016-07-19 01:57:13','2016-07-19 01:57:13',0),
(14,'HYP012','Killing Season','Kode9 & The Spaceape','2015-11-05','U.K.','Dubstep','Hyperdub',0,'Vinyl','12", 33 1/3 RPM. 1 x Black Vinyl.',12.00,1,'killing_season.jpg','2016-07-19 01:57:13','2016-07-19 01:57:13',0),
(15,'DTK11','12" EP (DTK11)','Dev/Null','2010-06-01','Canada','Breakcore','Dross:tik Records',0,'Vinyl','12", 33 1/3 RPM. 1 x Black Vinyl.',10.00,1,'dtk11.jpg','2016-07-19 01:57:13','2016-07-19 01:57:13',0),
(16,'DU26','Paralellabytes','bossFYTE','2015-08-25','Canada','Breakcore','Detroit Underground',0,'Vinyl','7" 45 RPM. 1 x Black Vinyl.',14.99,1,'paralellabytes.jpg','2016-07-19 01:57:13','2016-07-19 01:57:13',0),
(17,'ADN125EP','1/3 EP','Enduser','2010-03-01','Germany','Breakcore','Ad noiseam',0,'Vinyl','12", 33 1/3 RPM. 1 x Black Vinyl.',9.99,1,'1_3ep.jpg','2016-07-19 01:57:13','2016-07-19 01:57:13',0),
(18,'AMB1510LPLTD','Changes','Synkro','2015-09-18','U.K.','IDM',NULL,1,'Vinyl','12", 33 1/3 RPM. 1 x White Vinyl.',59.99,1,'changes.jpg','2016-07-19 01:57:13','2016-07-19 01:57:13',0),
(19,'DUKE067','NeoOuija','Metamatics','1998-10-07','U.K.','IDM','Hydrogen Dukebox Records',0,'Vinyl','12", 33 1/3 RPM. 1 x Black Vinyl.',24.99,1,'neoouija.jpg','2016-07-19 01:57:13','2016-07-19 01:57:13',0),
(20,'HIBIKI001','Monster Mashup','Various','2013-08-30','U.K.','IDM','Hibiki Records',0,'Vinyl','12", 33 1/3 RPM. 1 x Coloured Vinyl.',18.00,1,'monster_mashup.jpg','2016-07-19 01:57:13','2016-07-19 01:57:13',0);
