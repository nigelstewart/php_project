<?php

require_once 'inc/config.php';
require_once 'inc/functions.php';
require_once 'inc/ShoppingCart.php';

// Insantiate the shopping cart
$my_cart = new ShoppingCart();

$title = "Thank you";

// Get prices of cart items to calc line totals
if (!empty($my_cart->cart)) {
  $query = "SELECT product_id, price
            FROM product
            WHERE product_id
            IN (".implode(',',array_keys($my_cart->cart)).")";

  // Prepare query
  $stmt = $dbh->prepare($query);

  // Execute query
  $stmt->execute();

  // Fetch result
  $cart_items = $stmt->fetchAll(PDO::FETCH_ASSOC);
}

// Insert order into database
if ($_POST && !empty($_POST)){
  // Create query for invoice table
  $query = "INSERT INTO invoice
              (customer_id,
              first_name,
              last_name,
              email,
              address,
              city,
              postal_code,
              province,
              phone,
              card_digits,
              products_subtotal,
              tax,
              total_cost,
              order_date)
            VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,NOW())";

  // Create params
  $params = array(
    intval($_POST['customer_id']),
    filter_var($_POST['first_name'], FILTER_SANITIZE_STRING),
    filter_var($_POST['last_name'], FILTER_SANITIZE_STRING),
    filter_var($_POST['email'], FILTER_SANITIZE_EMAIL),
    filter_var($_POST['address'], FILTER_SANITIZE_STRING),
    filter_var($_POST['city'], FILTER_SANITIZE_STRING),
    filter_var($_POST['postal_code'], FILTER_SANITIZE_STRING),
    filter_var($_POST['province'], FILTER_SANITIZE_STRING),
    filter_var($_POST['phone'], FILTER_SANITIZE_NUMBER_INT),
    substr(filter_var($_POST['credit_card'], FILTER_SANITIZE_STRING), -4),
    floatval($_POST['subtotal']),
    floatval($_POST['tax']),
    floatval($_POST['total'])
  );

  // Prepare query
  $stmt = $dbh->prepare($query);

  //Check if execute query
  if($stmt->execute($params)){
    // Fetch order
    $order_number = $dbh->lastInsertId();

    //Insert products into invoice products table
    foreach($cart_items as $item) {
      // Create query
      $query = "INSERT INTO invoice_products
                (invoice_id,
                 product_id,
                 quantity,
                 price_per_unit,
                 line_total)
                VALUES (?,?,?,?,?)";

      $params = array(
        $order_number,
        $item['product_id'],
        $my_cart->cart[$item['product_id']],
        $item['price'],
        floatval($my_cart->cart[$item['product_id']] * $item['price'])
      );

      // Prepare query
      $stmt = $dbh->prepare($query);

      // Execute query
      if($stmt->execute($params)){
        $order_complete = true;
      }
    }
  }


  // Check if order complete flag is set so cart can be emptied
  if(isset($order_complete) && $order_complete == true){
    $my_cart->emptyCart();
  }

  // Get back order info
  if(empty($my_cart->cart)){
    // Create query
    $query = "SELECT * FROM invoice
              WHERE invoice_id = {$order_number}";

    // Prepare query
    $stmt = $dbh->prepare($query);

    // Execute query
    $stmt->execute();

    // Fetch result
    $invoice = $stmt->fetch(PDO::FETCH_ASSOC);
  }

}

include('inc/header.php'); ?>

  <div id="wrapper">

    <!-- Menu Include -->
    <?php include('inc/menu.php'); ?>

    <div id="content">
      <table class="order_details">
        <tr>
          <th colspan="2">Shipping &amp; order details</th>
        </tr>
        <tr>
          <td>Name:</td>
          <td><?=$invoice['first_name'] . ' ' . $invoice['last_name']?></td>
        </tr>
        <tr>
          <td>City:</td>
          <td><?=$invoice['city']?></td>
        </tr>
        <tr>
          <td>Province:</td>
          <td><?=$invoice['province']?></td>
        </tr>
        <tr>
          <td>Address:</td>
          <td><?=$invoice['address']?></td>
        </tr>
        <tr>
          <td>Postal Code:</td>
          <td><?=$invoice['postal_code']?></td>
        </tr>
        <tr>
          <td>Credit Card:</td>
          <td>************<?=$invoice['card_digits']?></td>
        </tr>
        <tr>
          <td>Order Number:</td>
          <td>0000000<?=$order_number?></td>
        </tr>

        <tr>
          <td>Subtotal:</td>
          <td>$<?=$invoice['products_subtotal']?></td>
        </tr>
        <tr>
          <td>Tax:</td>
          <td>$<?=$invoice['tax']?></td>
        </tr>
        <tr>
          <td>Total:</td>
          <td>$<?=$invoice['total_cost']?></td>
        </tr>
      </table>

      <div id="thanks">
        <p><strong>Thank you for your order!</strong></p>
        <p>You will recieve an email shortly confirming your order details.</p>
        <p>If you have any questions or concerns, please do not hesitate to contact us.</p>
        <p style="text-align: center">--Base 16 Records</p>
      </div>
    </div>
  </div>

<?php include('inc/footer.php'); ?>
