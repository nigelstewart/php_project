<?php

require_once 'inc/config.php';
require_once 'inc/functions.php';
require_once 'inc/ShoppingCart.php';

// Insantiate the shopping cart
$my_cart = new ShoppingCart();

$title = "Contact Us";

include('inc/header.php'); ?>

  <div id="wrapper">

    <!-- Menu Include -->
    <?php include('inc/menu.php'); ?>

    <div id="content">

    </div>

  </div>

<?php include('inc/footer.php'); ?>
