<?php

require_once 'inc/config.php';
require_once 'inc/functions.php';
require_once 'inc/ShoppingCart.php';

// Insantiate the shopping cart
$my_cart = new ShoppingCart();

// DATABASE QUERY
if (!empty($_GET)) {
  // Create query
  $query = "SELECT album_title,
                   sku,
                   label,
                   artist,
                   release_date,
                   country_released,
                   description,
                   genre,
                   price,
                   cover_img
            FROM product
            WHERE product_id = {$_GET['product_id']}";

  // Prepare query
  $stmt = $dbh->prepare($query);

  // Execute query
  $stmt->execute();

  // Fetch result
  $result = $stmt->fetch(PDO::FETCH_ASSOC);
}

if ($_SERVER['REQUEST_METHOD'] == 'POST'){
  $product_id = intval($_POST['id']);
  $quantity = intval($_POST['qty']);
  $my_cart->addItem($product_id, $quantity);
}

$title = $result['artist'] . ' - ' . $result['album_title'];

include('inc/header.php'); ?>

    <div id="wrapper">

      <!-- Menu Include -->
      <?php include('inc/menu.php'); ?>

      <div id="item_content">
        <img class="cover" src="img/album_cover/<?= $result['cover_img'] ?>" alt="Album Cover" />
        <table id="item_info">
          <tr><th>Price</th><td><span id="item_price">$<?= $result['price'] ?></span></td></tr>
          <tr><th>Title</th><td><?= $result['album_title'] ?></td></tr>
          <tr><th>Artist</th><td><?= $result['artist'] ?></td></tr>
          <tr><th>Year Released</th><td><?= $result['release_date'] ?></td></tr>
          <tr><th>Country Released</th><td><?= $result['country_released'] ?></td></tr>
          <tr><th>Description</th><td><?= $result['description'] ?></td></tr>
          <tr><th>Genre</th><td><?= $result['genre'] ?></td></tr>
          <tr><th>Label</th><td><?= $result['label'] ?></td></tr>
          <tr><th>SKU</th><td><?= $result['sku'] ?></td></tr>
        </table>
        <div id="add_item_wrapper">
          <form id="add_item_to_cart" action="#" method="post">
            <input type="submit" id="add_item" value="Add to Cart" />
            <input type="hidden" name="id" value="<?=$result['product_id']?>" />
            <select name="qty" id="detail_qty">
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
            </select>
          </form>
        </div>
      </div>

    </div>

<?php include('inc/footer.php'); ?>
