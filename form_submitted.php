<?php
require_once 'inc/config.php';
require_once 'inc/functions.php';
require_once 'inc/ShoppingCart.php';

// Insantiate the shopping cart
$my_cart = new ShoppingCart();

// Create new connection
$dbh = new PDO(
  'mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS
);

// Fetch cust_id cookie
$customer_id = $_COOKIE["cust_id"];

// Create query
$query = "SELECT first_name, last_name, address, city, postal_code, province,
                 country, phone, email
          FROM customer WHERE customer_id = $customer_id;";

// Prepare query
$stmt = $dbh->prepare($query);

// Execute query
$stmt->execute();

// Fetch results
$result = $stmt->fetchAll(\PDO::FETCH_ASSOC);

// Set title
$title = "Thank you";

include('inc/header.php'); ?>

  <div id="wrapper">

    <!-- Menu Include -->
    <?php include('inc/menu.php'); ?>

    <div id="content">
      <div id="submitted_info">
        <h1><?= $title ?> for registering!</h1>

        <p>You submitted the following information:</p>

        <?php foreach ($result[0] as $key=>$value) : ?>
          <?php $key = str_replace('_', ' ', $key); ?>
          <?php $key = ucwords($key); ?>
          <?php echo "<p>$key: <span class='post'>$value</span></p>"; ?>
        <?php endforeach; ?>

      </div>
    </div>

  </div>

<?php include('inc/footer.php'); ?>
