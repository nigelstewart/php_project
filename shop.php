<?php

require_once 'inc/config.php';
require_once 'inc/functions.php';
require_once 'inc/ShoppingCart.php';

// Insantiate the shopping cart
$my_cart = new ShoppingCart();

switch($_SERVER['REQUEST_URI']){
  case '/shop.php':
    $title = 'Shop';
    break;
  // case "/shop.php?search={$_GET['search']}":
  //   $title = 'Shop';
  //   break;
  case '/shop.php?format=vinyl':
    $title = 'Vinyl Shop';
    break;
  case '/shop.php?format=cassette':
    $title = 'Cassette Shop';
    break;
  case '/shop.php?genre=drumandbass':
    $title = 'Drum & Bass Shop';
    break;
  case '/shop.php?genre=breakcore':
    $title = 'Breakcore Shop';
    break;
  case '/shop.php?genre=dubstep':
    $title = 'Dubstep Shop';
    break;
  case '/shop.php?genre=idm':
    $title = 'IDM Shop';
    break;
  default:
    $title = 'Oops, something doesn\'t look right';
    break;
}

if (isset($_GET['search'])) {
  $title = 'Shop';
}

// DATABASE QUERIES
if (empty($_GET)) {
  // Create query
  $query = "SELECT album_title,
                   product_id,
                   sku,
                   artist,
                   description,
                   genre,
                   price,
                   cover_img
            FROM product
            WHERE deleted = 0";

    // Create params
    $params = array();

    // Prepare query
    $stmt = $dbh->prepare($query);

    // Execute query
    $stmt->execute($params);

    // Fetch result
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
} elseif (isset($_GET['format'])){
  // Get format
  $format = filter_var($_GET['format'], FILTER_SANITIZE_STRING);

  // Create query
  $query = "SELECT album_title,
                   product_id,
                   sku,
                   artist,
                   description,
                   genre,
                   price,
                   cover_img
            FROM product
            WHERE deleted = 0 AND format = '{$format}'";

    // Prepare query
    $stmt = $dbh->prepare($query);

    // Execute query
    $stmt->execute();

    // Fetch result
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
} elseif (isset($_GET['genre'])){
  // Get genre
  if ($_GET['genre'] == 'drumandbass') {
    $genre = 'drum & bass';
  } else { $genre = filter_var($_GET['genre'], FILTER_SANITIZE_STRING); }

  // Create query
  $query = "SELECT album_title,
                   product_id,
                   sku,
                   artist,
                   description,
                   genre,
                   price,
                   cover_img
            FROM product
            WHERE deleted = 0 AND genre = '{$genre}'";

    // Prepare query
    $stmt = $dbh->prepare($query);

    // Execute query
    $stmt->execute();

    // Fetch result
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
} elseif(filter_input(INPUT_GET, 'search')) {

  $search = filter_input(INPUT_GET, 'search', FILTER_SANITIZE_STRING);

  // Create query
  $query = "SELECT album_title,
                   product_id,
                   sku,
                   artist,
                   description,
                   genre,
                   price,
                   cover_img
            FROM product
            WHERE (artist LIKE :search) OR (album_title LIKE :search)";

    // Create params
    $params = array(
      ':search' => '%' . $search . '%'
    );

    // Prepare query
    $stmt = $dbh->prepare($query);

    // Execute query
    $stmt->execute($params);

    // Fetch result
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
} else {
  $error_message = "There was a problem with your request.";
}

if ($_SERVER['REQUEST_METHOD'] == 'POST'){
  $product_id = intval($_POST['id']);
  $quantity = intval($_POST['qty']);
  $my_cart->addItem($product_id, $quantity);
}

include('inc/header.php'); ?>

    <div id="wrapper">

      <!-- Menu Include -->
      <?php include('inc/menu.php'); ?>

      <?php if(isset($error_message)) : ?>
        <p id="error_msg"><?=$error_message?>
        <a href="shop.php">Back to shop</a></p>
      <?php endif; ?>

      <?php if(isset($results)) : ?>
        <div id="shop_items">
          <?php foreach($results as $row) : ?>
            <div class="product">
              <a href="item.php?product_id=<?=$row['product_id']?>">
                <img src="img/album_cover/<?=$row['cover_img']?>" alt="Album Cover" width="120" height="120"/>
              </a>
              <div class="product_info">
                <?= $row['description'] ?> <br />
                <?= $row['artist'] ?> <br />
                <?= $row['album_title'] ?> <br />
                <?= $row['genre'] ?> <br />
                <i><?= $row['sku'] ?></i>
              </div>
              <div class="addtocart">
                <p class="price">$<?= $row['price'] ?></p>
                <form id="add_item_to_cart" action="#" method="post">
                  <input type="hidden" name="id" value="<?=$row['product_id']?>" />
                  <select name="qty" id="qty">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                  </select>
                  <input type="submit" class="add_button" value="Add to Cart" />
                </form>
              </div>
            </div>
          <?php endforeach; ?>
          <?php if(isset($search) && count($results) == 1 ) : ?>
            <div id="result_text">Your  search for <strong><?=$search?></strong> returned <strong><?= count($results)?></strong> result.</div>
          <?php elseif(isset($search) && count($results) > 1 ) : ?>
            <div id="result_text">Your  search for <strong><?=$search?></strong> returned <strong><?= count($results)?></strong> results.</div>
          <?php elseif(isset($search) && count($results) < 1 ) : ?>
            <div id="result_text">Your search for <strong><?=$search?></strong> returned <strong>no result.</strong></div>
          <?php endif; ?>
        </div>
      <?php endif; ?>

      <!-- Pagination -->
      <!-- <div id="pages_nav">
        <a href="#">1</a>
        <a href="#">2</a>
        <a href="#">3</a>
        <a href="#">4</a>
        <a href="#">5</a>
      </div> -->
    </div>

<?php include('inc/footer.php'); ?>
